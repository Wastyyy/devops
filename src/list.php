<?php
// Définir le chemin du répertoire à lister
$directory = '/var/www';

// Obtenir la liste des fichiers et répertoires dans le répertoire
$fileList = scandir($directory);

// Afficher la liste des fichiers et répertoires
echo "<h2>Contenu de $directory :</h2>";
echo "<ul>";
foreach ($fileList as $file) {
    // Ignorer les entrées spéciales "." et ".."
    if ($file != "." && $file != "..") {
        echo "<li>$file</li>";
    }
}
echo "</ul>";
?>