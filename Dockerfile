FROM php:8.3-apache

# Install PHP extensions
RUN docker-php-ext-install pdo_mysql

# Copy application code
COPY src /var/www/html

RUN apt-get update \
   && apt install -y libgs9-common \
   && apt install -y libgs9 \
   && apt-get clean && rm -rf /var/lib/apt/lists/*

# Install dependencies
RUN apt-get update && apt-get install -y wget git unzip libpq-dev && \
    docker-php-ext-install pdo pdo_pgsql && \
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Set permissions for Apache
RUN chown -R www-data:www-data /var/www/html && \
    chmod -R 755 /var/www/html && \
    chown -R www-data:www-data /var/www && \
    chmod -R 755 /var/www


# Enable Apache modules
RUN a2enmod rewrite

# Expose port 80
EXPOSE 80

# Start Apache in the foreground
CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]